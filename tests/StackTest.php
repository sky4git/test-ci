<?php
namespace Tests;

use PHPUnit\Framework\TestCase;


class StackTest extends TestCase
{
  
    public function URLProvider() {
        return [
           // ["http://mel07web/test-ci/index.php"],
              ["tt.txt"],
              ["https://www.boroondara.vic.gov.au/"],
              ["https://eforms.boroondara.vic.gov.au/forms/form/178/en/book_a_waste_collection"]
        ];
    }
      
    /**
     * @dataProvider URLProvider
     */  
    public function test_url($url) {
        $data = file_get_contents( $url );
       // $data = shell_exec("curl -X POST $url -d");
        
       // $result = json_decode($data, true)['results'][0];
       switch( $url ){
           case 'tt.txt':
              $this->assertContains( 'me', $data );
           break;
           case 'https://www.boroondara.vic.gov.au/':
              $contains_what = '<a href="/about-council/payments" data-drupal-link-system-path="node/2951">Payments</a>';
              $this->assertContains( $contains_what, $data );  
           break; 
           case 'https://eforms.boroondara.vic.gov.au/forms/form/178/en/book_a_waste_collection':
            $contains_what = 'Book a waste collection';
            $this->assertContains( $contains_what, $data );
           break;
       }
       
    } 

    /**
     * @depends test_url
     */
    public function testHello(){

        $hooks = new \Hooks();
        $this->assertEquals( 'Hello', $hooks->sayHello(), 'SORRY' );

    } 

    /**
    * @depends testHello
    */
    public function testWorld(){

        $hooks = new \Hooks();
        //$this->assertContains('World', $hooks->printWorld(), 'SORRY');
        $this->expectOutputString('World', $hooks->printWorld(), 'SORRY');

    } 

}
?>